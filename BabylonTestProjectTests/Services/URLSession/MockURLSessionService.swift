@testable import BabylonTestProject
import RxSwift

class MockURLSessionService: URLSessionService {
    func reloadedData(request: URLRequest) -> Observable<Data> {
        fatalError("reloadedData(request:) has not been implemented")
    }
}
