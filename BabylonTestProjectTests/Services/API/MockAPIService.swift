@testable import BabylonTestProject
import RxSwift

class MockAPIService: APIService {

    var postsStream: Observable<[Post]> = .empty()

    func posts() -> Observable<[Post]> {
        return postsStream
    }

    func comments(of: Post) -> Observable<[Comment]> {
        fatalError("comments(of:) has not been implemented")
    }

    func user(id: Int) -> Observable<User> {
        fatalError("user(id:) has not been implemented")
    }
}
