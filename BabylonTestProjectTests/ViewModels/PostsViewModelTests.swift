@testable import BabylonTestProject
import XCTest
import Nimble
import RxSwift

enum TestError: Error {
    case test
}

class PostsViewModelTests: XCTestCase {

    let post = Post(userId: 0, id: 0, title: "title", body: "body")
    let viewModel = PostsViewModel()
    let mockAPIService = MockAPIService()

    override func setUp() {
        mock(mockAPIService)
    }

    func testPostsReturnsFetchedPosts() {
        mockAPIService.postsStream = .of([post])
        expect(self.viewModel.posts).array == [[post]]
    }

    func testPostsReturnsEmptyArrayOnError() {
        mockAPIService.postsStream = .error(TestError.test)
        expect(self.viewModel.posts).array == [[]]
    }
}

// ---------------------------------------

// this is experimental copy of RxNimble code, with support for `Driver` observable

@testable import RxNimble

public extension Expectation where T: ObservableConvertibleType {

    /**
     Expectation with sequence's first element

     Transforms the expression by blocking sequence and returns its first element.
     */
    public var first: Expectation<T.E> {
        return transform { source in
            try source?.toBlocking().first()
        }
    }
    /**
     Expectation with sequence's last element

     Transforms the expression by blocking sequence and returns its last element.
     */
    public var last: Expectation<T.E> {
        return transform { source in
            try source?.toBlocking().last()
        }
    }

    /**
     Expectation with all sequence's elements

     Transforms the expression by blocking sequence and returns its elements.
     */
    public var array: Expectation<[T.E]> {
        return transform { source in
            try source?.toBlocking().toArray()
        }
    }
}
