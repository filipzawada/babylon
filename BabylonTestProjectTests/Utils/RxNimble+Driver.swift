//@testable import RxNimble
//
//// this is a copy of RxNimble code, but with support for `Driver` observable
//
//public extension Expectation where T: ObservableConvertibleType {
//
//    /**
//     Expectation with sequence's first element
//
//     Transforms the expression by blocking sequence and returns its first element.
//     */
//    public var first: Expectation<T.E> {
//        return transform { source in
//            try source?.toBlocking().first()
//        }
//    }
//    /**
//     Expectation with sequence's last element
//
//     Transforms the expression by blocking sequence and returns its last element.
//     */
//    public var last: Expectation<T.E> {
//        return transform { source in
//            try source?.toBlocking().last()
//        }
//    }
//
//    /**
//     Expectation with all sequence's elements
//
//     Transforms the expression by blocking sequence and returns its elements.
//     */
//    public var array: Expectation<[T.E]> {
//        return transform { source in
//            try source?.toBlocking().toArray()
//        }
//    }
//}
