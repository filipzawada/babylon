import UIKit
import Hero

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let flow = MainFlow()

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions:
                             [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

#if DEBUG
        let path = "/Applications/InjectionX.app/Contents/Resources/iOSInjection.bundle"
        if let bundle = Bundle(path: path) {
            bundle.load()
            NotificationCenter.default.addObserver(
                    self,
                    selector: #selector(hotReload(_:)),
                    name: Notification.Name("INJECTION_BUNDLE_NOTIFICATION"),
                    object: nil
            )
        }
#endif

        // from docs:
        // > The response size is small enough to reasonably fit within the cache.
        // > (For example, if you provide a disk cache, the response must be no
        // > larger than about 5% of the disk cache size.)
        //
        // our largest request (comments) is 154 KiB + headers, so 10 MiB should be sufficient
        URLCache.shared = URLCache(memoryCapacity: 10 * .mibi,
                                   diskCapacity: 10 * .mibi,
                                   diskPath: nil)

        let nav = UINavigationController(rootViewController: flow.postsScreen.viewController)
        nav.hero.isEnabled = true

        window = UIWindow(frame: UIScreen.main.bounds).mutate {
            $0.rootViewController = nav
            $0.makeKeyAndVisible()
        }
        return true
    }

#if DEBUG
    @objc func hotReload(_ notification: Notification) {
        let nav = UINavigationController(rootViewController: flow.postsScreen.viewController)
        nav.hero.isEnabled = true

        window = UIWindow(frame: UIScreen.main.bounds).mutate {
            $0.rootViewController = nav
            $0.makeKeyAndVisible()
        }
    }
#endif
}
