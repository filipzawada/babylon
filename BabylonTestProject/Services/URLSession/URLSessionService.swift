import RxSwift

protocol URLSessionService {
    func reloadedData(request: URLRequest) -> Observable<Data>
}
