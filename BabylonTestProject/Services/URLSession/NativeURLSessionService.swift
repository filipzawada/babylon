import RxSwift

class NativeURLSessionService: URLSessionService {
    let urlSession = URLSession.shared

    /// queries first from cache (if available), then from network
    public func reloadedData(request: URLRequest) -> Observable<Data> {
        return urlSession.rx.reloadedData(request: request)
    }
}
