import Foundation

// discussion: naive approach to dependency injection, but should do for such a simple project.
//             E.g. I don't like having `mock` in prod-code project

private var apiService: APIService = RemoteAPIService()
func inject() -> APIService { return apiService }
func mock(_ inst: APIService) { apiService = inst }

private var urlSessionService: URLSessionService = NativeURLSessionService()
func inject() -> URLSessionService { return urlSessionService }
func mock(_ inst: URLSessionService) { urlSessionService = inst }
