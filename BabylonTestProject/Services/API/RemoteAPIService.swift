import RxSwift

class RemoteAPIService: APIService {

    // note to reviewer:
    //       urlSessionService is useful for mocking purposes if we
    //       were to test this `RemoteAPIService` class.
    private var urlSessionService: URLSessionService { return inject() }

    private let jsonDecoder = JSONDecoder()

    private let baseURL = URL(string: "https://jsonplaceholder.typicode.com")!
    private let baseComponents = URLComponents(string: "https://jsonplaceholder.typicode.com")!

    private func data<Type: Decodable & Equatable>(forPath path: String,
                                                  queryItems: [URLQueryItem] = [])
                    -> Observable<Type> {
        let components = baseComponents.mutate {
            $0.path = path
            $0.queryItems = queryItems
        }
        guard let url = components.url else {
            assert(false, "Could not resolve components url \(components)")
            // log error remotely
            return .empty()
        }
        return urlSessionService
                .reloadedData(request: URLRequest(url: url))
                .map { [jsonDecoder] in try jsonDecoder.decode(Type.self, from: $0) }
                .distinctUntilChanged(==)
    }

    func posts() -> Observable<[Post]> {
        return data(forPath: "/posts/")
    }

    func comments(of post: Post) -> Observable<[Comment]> {
        let postQueryItem = URLQueryItem(name: "postId", value: "\(post.id)")
        return data(forPath: "/comments/", queryItems: [postQueryItem])
    }

    func user(id: Int) -> Observable<User> {
        return data(forPath: "/users/\(id)")
    }
}
