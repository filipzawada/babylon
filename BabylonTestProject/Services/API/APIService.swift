import RxSwift

protocol APIService {
    func posts() -> Observable<[Post]>
    func comments(of: Post) -> Observable<[Comment]>
    func user(id: Int) -> Observable<User>
}
