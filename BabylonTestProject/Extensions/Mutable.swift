import UIKit

/// Lenses (swift version)
/// credits: https://github.com/ReactiveX/RxSwift/blob/master/RxExample/RxExample/Lenses.swift
protocol Mutable {}

extension Mutable {
    func mutateOne<T>(transform: (inout Self) -> T) -> Self {
        var newSelf = self
        _ = transform(&newSelf)
        return newSelf
    }

    func mutate(transform: (inout Self) -> ()) -> Self {
        var newSelf = self
        transform(&newSelf)
        return newSelf
    }

    func mutate(transform: (inout Self) throws -> ()) rethrows -> Self {
        var newSelf = self
        try transform(&newSelf)
        return newSelf
    }
}

extension URLComponents: Mutable {}
extension URLRequest: Mutable {}
extension UIWindow: Mutable {}
