
extension Int {
    /// Kibi, as in KiB - KibiByte = 1024 bytes
    static let kibi: Int = 1 << 10
    /// Mibi, as in MiB - MibiByte = 1024 * 1024 bytes
    static let mibi: Int = 1 << 20
}
