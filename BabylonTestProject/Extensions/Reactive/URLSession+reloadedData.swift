import RxSwift
import RxCocoa

// todo: that's how RxSwift added their json(...) operator. I'm not sure frankly
//       how did they test it (didn't find in sources), but I hope there's some
//       tricky way. Otherwise we can move it to a service and use mocks for testing
//
// discussion: In a hindsight I think this operator is not a great idea. It hides
//             too many edge cases, and if we were to expose them, it'd be too complex,
//             But leaving it here, since it works for this simple app.
extension Reactive where Base: URLSession {

    /// queries first from cache (if available), then from network
    /// - never returns an error, just finishes silently
    public func reloadedData(request: URLRequest) -> Observable<Data> {
        assert(request.cachePolicy == .useProtocolCachePolicy,
               "reloadData(request:) assumes default cache policy is used. " +
               "Other scenarios are yet to be implemented")

        let cachedRequest = request.mutate { $0.cachePolicy = .returnCacheDataDontLoad }

        let cacheStream = data(request: cachedRequest)
                .catchError { _ in .empty() } // skip not-in-cache errors
        let liveStream = data(request: request)
                .catchError { _ in .empty() } // terminate final stream without error

        return .concat(cacheStream, liveStream)
    }
}
