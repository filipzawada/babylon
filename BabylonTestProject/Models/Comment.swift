import Foundation

struct Comment: Codable, Equatable {
    let postId: Int
    let id: Int
}
