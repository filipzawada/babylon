import Foundation

struct User: Codable, Equatable {
    let id: Int
    let name: String
}
