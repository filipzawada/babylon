import FlowKit

class MainFlow {
    lazy var postsScreen = Flow(with: PostsViewController()) { [unowned self] vc, lets in
        vc.onCellSelect = lets.push(self.postDetailScreen) { $0.prepare }
    }

    lazy var postDetailScreen = Flow(with: PostDetailViewController()) { [unowned self] vc, lets in
        vc.onBack = lets.pop()
    }
}
