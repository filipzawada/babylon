import UIKit
import RxSwift
import RxCocoa
import Hero

class PostDetailViewController: UIViewController {

    private let disposeBag = DisposeBag()
    private let model = PostDetailViewModel()

    let authorLabel = UILabel()
    let titleLabel = UILabel()
    let bodyLabel = UILabel()
    let commentsCountLabel = UILabel()
    let stackView = UIStackView()

    var onBack: () -> Void = {}

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Details"

        hero.isEnabled = true
        view.backgroundColor = .white

        addStackView()
        installBindings()
    }

    func addStackView() {
        view.addSubview(stackView)
        stackView.snp.makeConstraints { $0.edges.equalTo(view.safeAreaLayoutGuide) }

        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fillProportionally
        stackView.spacing = 0

        // .cascade doesn't work on stack-view for some reason 🤔
        stackView.hero.modifiers = [.cascade]

        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(bodyLabel)
        stackView.addArrangedSubview(commentsCountLabel)
        stackView.addArrangedSubview(authorLabel)
        stackView.hero.modifiers = [.cascade]

        authorLabel.textAlignment = .center
        authorLabel.backgroundColor = .darkGray
        authorLabel.textColor = .lightGray
        authorLabel.hero.modifiers = [.fade, .scale(0.5), .translate(x: 100)]

        bodyLabel.numberOfLines = 0
        bodyLabel.textAlignment = .center
        bodyLabel.backgroundColor = .gray
        bodyLabel.hero.modifiers = [.fade, .scale(0.5), .translate(x: 200)]

        titleLabel.numberOfLines = 1
        titleLabel.textAlignment = .center
        titleLabel.font = .preferredFont(forTextStyle: .headline)
        titleLabel.backgroundColor = .white
        titleLabel.hero.id = HeroID.title
        titleLabel.snp.makeConstraints { $0.height.equalTo(40) }

        commentsCountLabel.textAlignment = .center
        commentsCountLabel.textColor = .gray
        commentsCountLabel.hero.modifiers = [.fade, .scale(0.5), .translate(x: 300)]
    }

    func prepare(with post: Post) {
        // view owns a model (so it marks it private).
        // That's why we need this unfortunate proxy method.
        model.prepare(with: post)
    }

    func installBindings() {
        model.title
             .drive(titleLabel.rx.text)
             .disposed(by: disposeBag)

        model.body
             .drive(bodyLabel.rx.text)
             .disposed(by: disposeBag)

        model.commentsCount
             .drive(commentsCountLabel.rx.text)
             .disposed(by: disposeBag)

        model.username
             .drive(authorLabel.rx.text)
             .disposed(by: disposeBag)
    }
}
