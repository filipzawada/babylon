import RxCocoa
import RxSwift

private extension String {
    /// keeps label visible in stackview, but without string content
    static let invisible = " "
}

class PostDetailViewModel {

    private var apiService: APIService { return inject() }

    private let postVariable = Variable<Post?>(nil)

    // note to reviewer:
    // I also used a dedicated `struct`s to separate
    // `Input` from `Output` in bigger projects.

    //MARK: output
    var title: Driver<String> {
        return postVariable.asDriver()
                           .map { $0?.title ?? .invisible }
    }

    var body: Driver<String> {
        return postVariable.asDriver()
                           .map { $0?.body ?? .invisible }
    }

    // note to reviewer:
    // ok, so I deliberately complicated this example with an assumption
    // that postVariable can change over time, which is not the case.

    var commentsCount: Driver<String> {
        return postVariable
                .asObservable()
                .flatMapLatest { [unowned self] (post: Post?) -> Observable<String> in
                    guard let post = post else {
                        return .just(.invisible)
                    }
                    return self.apiService
                               .comments(of: post)
                               .map { "comments count: \($0.count)" }
                }
                .startWith(.invisible)
                .asDriver(onErrorJustReturn: .invisible)
    }

    var username: Driver<String> {
        return postVariable
                .asObservable()
                .flatMapLatest { [unowned self] (post: Post?) -> Observable<String> in
                    guard let post = post else {
                        return .just(.invisible)
                    }
                    return self.apiService
                               .user(id: post.userId)
                               .map { "by \($0.name)" }
                }
                .startWith(.invisible)
                .asDriver(onErrorJustReturn: String.invisible)
    }

    //MARK: input
    func prepare(with post: Post) {
        postVariable.value = post
    }
}