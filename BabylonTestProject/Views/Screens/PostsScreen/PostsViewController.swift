import UIKit
import RxCocoa
import RxSwift
import SnapKit
import Hero

class PostsViewController: UIViewController {

    private let model = PostsViewModel()
    private let disposeBag = DisposeBag()

    private let tableView = UITableView()

    var onCellSelect: (Post) -> Void = { _ in }

    override func viewDidLoad() {
        super.viewDidLoad()

        hero.isEnabled = true
        title = "Posts"

        view.addSubview(tableView)

        tableView.snp.makeConstraints { $0.edges.equalToSuperview() }
        tableView.register(Cell.self, forCellReuseIdentifier: Cell.id)

        installBindings()
    }

    func installBindings() {
        model.posts
             .drive(tableView.rx.items(cellIdentifier: Cell.id)) { _, post, cell in
                 cell.textLabel?.text = post.title
                 cell.textLabel?.hero.id = HeroID.title
             }
             .disposed(by: disposeBag)

        // being dogmatic we should go through view-model,
        // but this is not very practical
        tableView.rx.modelSelected(Post.self)
                    .subscribe(onNext: { [weak self] in
                        self?.onCellSelect($0)
                    })
                    .disposed(by: disposeBag)
    }
}

private class Cell: UITableViewCell {
    static let id = "Cell"
}
