import RxSwift
import RxCocoa

class PostsViewModel {

    private var apiService: APIService { return inject() }

    var posts: Driver<[Post]> {
        return apiService.posts()
                         .asDriver(onErrorJustReturn: [])
    }
}
